import java.io.PrintStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

import com.sun.prism.paint.Color;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;


public class Main extends Application{
	static ArrayList <Thread> threads = new ArrayList<Thread>(); // max 4
	static ArrayList <CameraOBJ> cameras = new ArrayList<CameraOBJ>(); // max 4
	
	static Stage window;
	
	static Scene wrapper;
	static Scene login;
    static Scene register;
    
	static String username;
	static String password;
	
	static GridPane gp;
	
	public static void main(String[] args) {
		launch(args);
	}
	
	public void start(Stage stage) {
		window = stage;
		window.setResizable(false);
		window.setTitle("Control Room");
		
		BorderPane bp = new BorderPane();
		bp.setPadding(new Insets(10,10,10,10));
		bp.setId("Main_BP");
		bp.setMinSize(1366, 768);
		bp.setCenter(setCenter());
		bp.setRight(setRight());
		
	    
	    
		Scene sc = new Scene(bp);
		
		window.setScene(sc);
		window.show();
	}
	
	public static GridPane setCenter(){
		gp = new GridPane();
		gp.setHgap(10);
		gp.setVgap(10);
		for(int i=1; i<5;i++){
			try{
				gp.add(cameras.get(i-1).sp, (i%2==0?1:0),(i<3?0:1) );
			}catch(Exception e){
				StackPane empty = new StackPane();
				empty.setMinSize(500, 384);
				empty.setMaxSize(500, 384);
				empty.setStyle("-fx-background-color: #FFFFFF;");
				
				Button b = new Button("Add Camera");
				if(i==1){
					b.setOnAction(par->{
						popUpMsg(0);
						System.out.println(0);
					});
				}else if(i==2){
					b.setOnAction(par->{
						popUpMsg(1);
						System.out.println(1);
					});
				}else if(i==3){
					b.setOnAction(par->{
						popUpMsg(2);
						System.out.println(2);
					});
				}else if(i==4){
					b.setOnAction(par->{
						popUpMsg(3);
						System.out.println(3);
					});
				}
				
				empty.setAlignment(b,Pos.CENTER);
				empty.getChildren().add(b);
				
				gp.add(empty, (i%2==0?1:0), (i<3?0:1));
			}
		}
		return gp;
	}
	
	public static VBox setRight(){
		VBox settings = new VBox(5);
		settings.setStyle("-fx-background-color: #111111;");
		settings.setMinSize(455, 768);
		settings.setMaxSize(455, 768);
		
		//settings.getChildren().addAll(setCameraSetting(cameras.get(0)),setRoomSetting(),setConsole());
		
		return settings;
	}
	
	public static VBox setCameraSetting(CameraOBJ obj){
		VBox camera = new VBox(10);
		camera.setStyle("-fx-background-color: #555555;");
		camera.setMinSize(455, 256);
		camera.setMaxSize(455, 256);
		return camera;
	}
	
	public static VBox setRoomSetting(){
		VBox room = new VBox(10);
		room.setStyle("-fx-background-color: #999999;");
		room.setMinSize(455, 256);
		room.setMaxSize(455, 256);
		return room;
	}
	public static VBox setConsole(){
		VBox console = new VBox(10);
		console.setStyle("-fx-background-color: #FFFFFF;");
		console.setMinSize(455, 256);
		console.setMaxSize(455, 256);
		return console;
	}
	
	public static void setWindow(Scene s){
    	window.setScene(s);
    	window.centerOnScreen();
    	window.show();
    }
	
	public static void popUpMsg(int num){
    	Stage popup = new Stage();
    	popup.centerOnScreen();
    	popup.initModality(Modality.APPLICATION_MODAL);
    	popup.setTitle("Add Camera");
    	popup.initStyle(StageStyle.UNDECORATED);
    	
    	Label name_lbl = new Label("Camera name:");
    	TextField name_txt = new TextField();
    	Label ip_lbl = new Label("Camera name:");
    	TextField ip_txt = new TextField();
    	Label port_lbl = new Label("Camera name:");
    	TextField port_txt = new TextField();
    	
    	Button saveButton = new Button("Save");
    	saveButton.setOnAction(e -> {
    		addCamera(num,name_txt.getText(),ip_txt.getText(),port_txt.getText());
    		
    		popup.close();
    	});
    	
    	Button closeButton = new Button("Close");
    	closeButton.setOnAction(e -> {
    		popup.close();
    	});
    	
    	VBox layout = new VBox(10);
    	layout.setPadding(new Insets(20,20,20,20));
    	
    	layout.getChildren().addAll(name_lbl,name_txt,ip_lbl,ip_txt,port_lbl,port_txt,saveButton,closeButton);
    	layout.setAlignment(Pos.CENTER);
    	layout.setMinSize(200,150);
    	
    	Scene scene = new Scene(layout);
    	popup.setScene(scene);
    	popup.showAndWait();
    }  
	
	public static void addCamera(int num,String name,String ip,String port){
		int p = Integer.parseInt(port);
		cameras.add(num, new CameraOBJ(name,ip,p));
		threads.add(new Thread(cameras.get(num)));
		threads.get(num).start();
		gp.add(cameras.get(num++).sp, (num%2==0?1:0), (num<3?0:1));
	}
	
	@SuppressWarnings("deprecation")
	public static void removeCamera(int num){
		threads.get(num).stop();
		threads.remove(num);
		cameras.get(num).terminateConnection();
		cameras.remove(num);
		
	}
}
