import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;

import javafx.animation.AnimationTimer;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;


public class CameraOBJ implements Runnable{
	String name;
	Socket socket;
	String ip;
	int port;
	int delay;
	
	PrintStream ps;
	Scanner sc;
	
	StackPane sp;
	Label delay_lbl;
	ImageView iv;
	
	int coutner = 0;
	
	public CameraOBJ (String name, String ip, int port){
		this.ip = ip;
		this.port = port;
		int delay = 999;
		this.name = name;
		sp = new StackPane();
	}
	
	public void run() {
		connect();
		setStackPane();
		Timeline timeline = new Timeline();
		Duration duration = Duration.seconds(1/Settings.FPS);
		KeyFrame f1 = new KeyFrame(duration,e->{
			updateFrame();
		});
		timeline.getKeyFrames().add(f1);
		timeline.setCycleCount(Timeline.INDEFINITE);
	    timeline.play();
    }
	
	public void setStackPane(){
		Platform.runLater(new Runnable() { 
			public void run() {
				sp.setMinSize(500, 384);
				sp.setMaxSize(500, 384);
				sp.setStyle("-fx-background-color: #FFFFFF;");
				Image image = new Image("Camera_Frame.jpg");
				iv = new ImageView(image);
			
				Label name_lbl = new Label(name);
				delay_lbl = new Label(delay+"");
			
				sp.setAlignment(iv,Pos.CENTER);
				sp.setAlignment(name_lbl,Pos.TOP_LEFT);
				sp.setAlignment(delay_lbl,Pos.BOTTOM_RIGHT);
			
				sp.getChildren().addAll(iv,name_lbl,delay_lbl);                
			}
		});
	}
	
	public void updateFrame(){
		Image i = new Image(coutner+++".png");
		iv.setImage(i);
		delay_lbl.setText(getDelay());
	}
	
	public void connect(){
		try
        {   
            socket = new Socket(ip, port);
            
            ps = new PrintStream(socket.getOutputStream());
            sc = new Scanner(socket.getInputStream());
            
            sendMessage("1100");
            
            System.out.println(sc.nextLine());
        }
        catch (Exception c)
        {
            System.out.println(c.toString());
        }
	}
	
	public void sendMessage(String message){
		ps.println(message);
	}
	
	public void terminateConnection(){
		ps.close();
        try {
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String getDelay(){
		return (delay++)+"";
	}
}
